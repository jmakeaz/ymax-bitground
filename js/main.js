$(document).ready(function () {

    // main: slider
    $('.js-main-slider').slick({
        vertical: true,
        dots: true,
        speed: 300,
        autoplay: true,
        arrows: false,
        appendDots: $('.slider-control'),
        customPaging: function (slider, index) {
            var title = $(slider.$slides[index]).data('title') || '';
            return '<button type="button" class="slider-control-button">' + title + '</button>';
        },
    });

    // 게시판 페이지: slider
    $('.js-banner-slider').slick({
        dots: true,
        speed: 100,
        autoplay: true,
        arrows: false,
        appendDots: $('.banner-control'),
        customPaging: function (slider, index) {
            var title = $(slider.$slides[index]).data('title') || '';
            return '<div class="banner-control-item"><span class="banner-control-text">' + title + '</span><button type="button" aria-label="' + title + '" class="banner-control-button"></button></div>';
        },
    });
});

$(window).resize(function () {
    var $searchCombo = $('.jsSearchCombo');
    if ($searchCombo.length && $searchCombo.attr('aria-expanded') === 'true') {
        searchOptionPosition();
    }
});

$(window).scroll(function () {

    // Page - Terms of Service 
    var termsContents = $('.js-terms');
    var termsNavi = termsContents.find('.terms-list');

    if ($('body').has(termsContents).length) {
        if ($(window).scrollTop() > termsContents.offset().top) {
            termsNavi.css({
                position: 'fixed',
                top: 0
            });
        } else {
            termsNavi.css({
                position: 'absolute',
                top: 0
            });
        }
    }
});

// body 클릭했을 때 레이어 닫힘
$('body').on('click', function (e) {

    // 게시판 검색 역역 > 셀렉트 박스 옵션 목록 닫기
    var $searchFieldCombobox = $('.c-search-select');
    if (!$searchFieldCombobox.is(e.target) && $searchFieldCombobox.has(e.target).length === 0) {
        $searchFieldCombobox.append($('.jsSearchOptions'));
        $searchFieldCombobox.find('.jsSearchCombo').attr('aria-expanded', 'false')
    }
});

// main: transaction amount tab
$('.js-transaction-tab').on('click', function (e) {
    var $tabGroup = $(this);
    var $tab = $(e.target);

    if ($tab.attr('role') !== 'tab' || $tab.hasClass('active')) return;

    // tab
    $tab.addClass('active').siblings().removeClass('active');

    // content
    $tabGroup.siblings('[role=tabpanel]').hide();
    $('.' + $tab.data('controls')).show();
});

// 게시판 검색 영역 > 셀렉트 박스 열고 닫기
$('.jsSearchCombo').on('click', function () {
    var $this = $(this);
    var $options = $('.jsSearchOptions');

    if ($this.attr('aria-expanded') !== 'true') {
        // show options
        $this.attr('aria-expanded', 'true');
        $('body').append($options);
        searchOptionPosition();
    } else {
        // hide options
        $this.parent().append($options);
        $this.attr('aria-expanded', 'false');
    }
});

// 게시판 검색 영역 > 셀렉트 박스 옵션 리스트 위치
function searchOptionPosition() {
    var $combobox = $('.jsSearchCombo');
    var $options = $('.jsSearchOptions');
    $options.css({
        top: $combobox.offset().top + 43,
        left: $combobox.offset().left,
    });
}

// 게시판 검색영역 셀렉트 박스 > 옵션 선택
$('.jsSearchOptions').on('click', 'li', function () {
    var $this = $(this);
    var $combobox = $('.c-search-select');
    var $renderer = $combobox.find('.c-combobox-selection');
    var selectedValue = $this.attr('data-value');
    var selectedText = $this.text();

    $this.attr('aria-selected', 'true').siblings().attr('aria-selected', 'false');
    $renderer.text(selectedText).attr('data-value', selectedValue).attr('aria-expanded', 'false');
});

// 게시판 검색 영역 열고 닫기
$('.jsSearchButton').on('click', function () {
    var $this = $(this);
    var $parents = $this.closest('.c-search');
    var isActive = $parents.hasClass('active');

    if (!isActive) {
        $parents.addClass('active');
    } else {
        $parents.removeClass('active');
    }
});

$('.js-faq-tab').on('click', '.faq-tab', function () {
    var $tab = $(this);
    var $tabGroup = $tab.parent();

    // tab
    $tab.addClass('active').siblings().removeClass('active');

    // content
    $tabGroup.siblings('[role=tabpanel]').hide();
    $('.' + $tab.data('controls')).show();
});

// FAQ 리스트 열고 닫기
$('.js-faq-list').on('click', '.faq-item-question', function () {
    var $question = $(this);
    var $answer = $question.next();

    if ($question.parent().hasClass('active')) {
        $question.parent().removeClass('active');
        $answer.slideUp(200);
    } else {
        $question.parent().addClass('active');
        $answer.slideDown(300);
    }
});

// terms 타이틀 리스트 클릭했을 때
$('.js-terms-navi').on('click', 'li', function (e) {
    e.preventDefault();
    var $this = $(this);

    if ($this.hasClass('active')) return;
    $this.addClass('active').siblings().removeClass('active');
    $('html').animate({
        scrollTop: $('.terms-content').find('.terms-title').eq($this.index()).offset().top - 34
    }, 300);
});