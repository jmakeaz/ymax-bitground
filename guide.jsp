<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Guide - bitground</title>
    <meta name="description" content="">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

    <link rel="manifest" href="/bitground/site.webmanifest">
    <link rel="apple-touch-icon" href="/bitground/icon.png">
    <link rel="shortcut icon" href="/bitground/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/bitground/css/base.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/common.css">
</head>

<body>
    <jsp:include page="/bitground/include/header.jsp" flush="true"></jsp:include>

    <div class="container">
        <div class="wrapper">
            <div style="padding: 0 200px 100px;">
                <h2 style="margin-top: 100px; margin-bottom: 20px; font-size: 24px; border-bottom: 1px solid #f48c19; line-height: 2; color: #163555;">Title</h2>
                <h3 class="c-page-title">Sign Up</h3>
                <hr style="margin: 10px 0;">
                <h3 class="c-page-title">Sign in</h3>
                <h4 class="c-page-sub-title">Verification</h4>

                <h2 style="margin-top: 100px; margin-bottom: 20px; font-size: 24px; border-bottom: 1px solid #f48c19; line-height: 2; color: #163555;">Icon</h2>
                <span class="sprite-direction arrow-double-to-left"></span>
                <span class="sprite-direction arrow-double-to-left-on"></span>
                <span class="sprite-direction arrow-double-to-right"></span>
                <span class="sprite-direction arrow-double-to-right-on"></span>
                <span class="sprite-direction arrow-to-bottom"></span>
                <span class="sprite-direction arrow-to-left"></span>
                <span class="sprite-direction arrow-to-left-on"></span>
                <span class="sprite-direction arrow-to-right"></span>
                <span class="sprite-direction arrow-to-right-on"></span>
                <span class="sprite-direction arrow-to-top"></span>
                <hr style="margin: 10px 0;">
                <span class="sprite-icon avatar"></span>
                <span class="sprite-icon avatar-black"></span>
                <span class="sprite-icon digit"></span>
                <span class="sprite-icon digit-black"></span>
                <span class="sprite-icon download"></span>
                <span class="sprite-icon email"></span>
                <span class="sprite-icon email-black"></span>
                <span class="sprite-icon gender"></span>
                <span class="sprite-icon password"></span>
                <span class="sprite-icon password-black"></span>
                <span class="sprite-icon search"></span>
                <span class="sprite-icon search-on"></span>
                <span class="sprite-icon verify-code"></span>
                <span class="sprite-icon verify-code-black"></span>
                <span class="sprite-icon caution-blue"></span>
                <span class="sprite-icon caution-red"></span>
                <span class="sprite-icon warning"></span>
                <span class="sprite-icon phone"></span>
                <hr style="margin: 10px 0;">
                <div style="background-color: antiquewhite">
                    <p>image</p>
                    <img src="/bitground/img/sprite-icon.png" alt="">
                </div>

                <h2 style="margin-top: 100px; margin-bottom: 20px; font-size: 24px; border-bottom: 1px solid #f48c19; line-height: 2; color: #163555;">Button</h2>
                <button type="button" class="c-button">button</button>
                <a href="#" class="c-button">a button</a>
                <button type="button" class="c-button secondary">button</button>
                <button type="button" class="c-button primary">button</button>
                <button type="button" class="c-button transparent">button</button>
                <button type="button" class="c-button line">button</button>
                <button type="button" class="c-button medium">button</button>

                <h2 style="margin-top: 100px; margin-bottom: 20px; font-size: 24px; border-bottom: 1px solid #f48c19; line-height: 2; color: #163555;">Checkbox</h2>
                <p class="c-checkbox signup-agree">
                    <input type="checkbox" id="agreeThat" class="visuallyhidden">
                    <label for="agreeThat" class="c-checkbox-label">
                        I agree to bitground's
                        <a href="#">Terms of Service</a> and
                        <a href="#">Privacy Policy</a>
                        <span class="c-required">*<span class="visuallyhidden">required</span></span>
                    </label>
                </p>

                <h2 style="margin-top: 100px; margin-bottom: 20px; font-size: 24px; border-bottom: 1px solid #f48c19; line-height: 2; color: #163555;">Validation
                    message</h2>
                <p class="c-invalid-message">Please agree to the Terms of Service and the privacy policy.</p>

                <h2 style="margin-top: 100px; margin-bottom: 20px; font-size: 24px; border-bottom: 1px solid #f48c19; line-height: 2; color: #163555;">Input</h2>
                <p class="input-field">
                    <label for="userName" class="input-field-label">
                        <span class="sprite-icon avatar"></span>
                        User Name
                        <span class="c-required">*<span class="visuallyhidden">required</span></span>
                    </label>
                    <span class="input-field-input">
                        <input type="text" id="userName" value="" placeholder="Enter your name">
                    </span>
                    <!-- <strong class="input-field-message">This email is already in use.</strong> -->
                </p>
                <p class="input-field">
                    <label for="sex" class="input-field-label">
                        <span class="sprite-icon gender"></span>
                        Gender
                        <span class="c-required">*<span class="visuallyhidden">required</span></span>
                    </label>
                    <span class="input-field-input">
                        <span class="c-select">
                            <select id="sex">
                                <option value="">Male</option>
                                <option value="">Female</option>
                            </select>
                        </span>
                    </span>
                    <!-- <strong class="input-field-message">This email is already in use.</strong> -->
                </p>

                <!-- validation error났을 때 "invalid" 클래스명 추가 -->
                <p class="input-field invalid">
                    <label for="email" class="input-field-label">
                        <span class="sprite-icon email"></span>
                        Email
                        <span class="c-required">*<span class="visuallyhidden">required</span></span>
                    </label>
                    <span class="input-field-input">
                        <input type="email" id="email" value="neo@gmail.com" placeholder="Enter your email address"
                               autocomplete="username">
                    </span>
                    <strong class="input-field-message">This email is already in use.</strong>
                </p>


                <h2 style="margin-top: 100px; margin-bottom: 20px; font-size: 24px; border-bottom: 1px solid #f48c19; line-height: 2; color: #163555;">Text
                    Color</h2>
                <p class="t-primary">Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum impedit, earum
                    officia veritatis, nulla vero quisquam odit fugit repudiandae enim sapiente quae necessitatibus!
                    Iste illo et, nisi ab cumque quam?</p>
                <p class="t-secondary">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi accusamus sequi
                    soluta enim nobis natus praesentium, officia eligendi, voluptates blanditiis nihil quae quos
                    aspernatur expedita voluptatem culpa facere consequuntur quia!</p>

            </div>
        </div>
    </div>

    <jsp:include page="/bitground/include/footer.jsp" flush="true"></jsp:include>

    <script src="/bitground/js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/main.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>