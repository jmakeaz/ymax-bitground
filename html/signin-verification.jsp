<!doctype html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>bitground</title>
    <meta name="description" content="">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

    <link rel="manifest" href="/bitground/site.webmanifest">
    <link rel="apple-touch-icon" href="/bitground/icon.png">
    <link rel="shortcut icon" href="/bitground/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/bitground/css/base.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/common.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/member.css">
</head>

<body>
    <jsp:include page="/bitground/include/header.jsp" flush="true"></jsp:include>

    <div class="container">
        <div class="wrapper">
            <h2 class="visuallyhidden">Main Contents</h2>

            <div class="contents contents-member">
                <h3 class="c-page-title">Sign In</h3>
                <h4 class="c-page-sub-title">Verification</h4>

                <form action="#">
                    <fieldset>
                        <div class="top-message-line">
                            <p class="c-validation-message">We sent the verify code to your email.</p>

                            <!-- 코드를 잘못 입력한 경우 -->
                            <!-- <p class="c-validation-message invalid">You entered the wrong code.</p> -->

                            <!-- 이메일을 통한 링크 인증이 완료되지 않은 경우 -->
                            <!-- <p class="c-validation-message invalid">Please authenticate to the link sent through your
                                email.</p> -->
                        </div>

                        <p class="input-field">
                            <label for="verifyCode" class="input-field-label">
                                <span class="sprite-icon verify-code"></span>
                                Verify code
                            </label>
                            <span class="input-field-input">
                                <input type="number" id="verifyCode" value="" placeholder="Enter code to your email">
                            </span>
                        </p>

                        <div class="button-wrap">
                            <button class="c-button medium signin-verification-button">Send</button>
                        </div>
                    </fieldset>
                </form>
            </div>

        </div>
    </div>

    <jsp:include page="/bitground/include/footer.jsp" flush="true"></jsp:include>

    <script src="/bitground/js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/main.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>