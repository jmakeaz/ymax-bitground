<!doctype html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>bitground</title>
    <meta name="description" content="">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

    <link rel="manifest" href="/bitground/site.webmanifest">
    <link rel="apple-touch-icon" href="/bitground/icon.png">
    <link rel="shortcut icon" href="/bitground/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/bitground/css/base.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/common.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/member.css">
</head>

<body>
    <jsp:include page="/bitground/include/header.jsp" flush="true"></jsp:include>

    <div class="container">
        <div class="wrapper">
            <h2 class="visuallyhidden">Main Contents</h2>

            <div class="contents contents-member">
                <h3 class="c-page-title">Sign In</h3>

                <form action="#">
                    <fieldset>
                        <div class="top-message-line">
                            <!-- 로그인 실패 메세지 -->
                            <!-- <p class="c-validation-message invalid">Wrong customer email or password, please try again.</p>
                            <p class="c-validation-message invalid">The 6-digit Two-Factor Authentication code you
                                entered does not match.</p> -->

                            <!-- 비밀번호 재설정 후 메세지 -->
                            <p class="c-validation-message">Login password has been reset, please keep your new
                                password.</p>
                        </div>

                        <p class="input-field">
                            <label for="email" class="input-field-label">
                                <span class="sprite-icon email"></span>
                                Email
                            </label>
                            <span class="input-field-input">
                                <input type="email" id="email" value="" placeholder="Enter your email address"
                                       autocomplete="username">
                            </span>
                        </p>
                        <p class="input-field">
                            <label for="password" class="input-field-label">
                                <span class="sprite-icon password"></span>
                                Password
                            </label>
                            <span class="input-field-input">
                                <input type="password" id="password" value="" placeholder="Enter password" autocomplete="current-password">
                            </span>
                        </p>
                        <p class="input-field">
                            <label for="digitCode" class="input-field-label">
                                <span class="sprite-icon digit"></span>
                                6-digit code
                            </label>
                            <span class="input-field-input">
                                <input type="number" id="digitCode" value="" placeholder="Enter Two-Factor Authentication code (if enabled)">
                            </span>
                        </p>

                        <div class="button-wrap button-wrap-signin">
                            <a href="/bitground/html/password1.jsp" class="c-button medium transparent">Forgot
                                Password?</a>
                            <button class="c-button medium">Sign in</button>
                        </div>
                    </fieldset>
                </form>

                <h4 class="signin-advice">Don’t have an account?</h4>
                <p class="signin-advice-desc">Create one to start trading on the world’s most active digital asset
                    exchange.</p>
                <a href="/bitground/html/signup.jsp" class="c-button medium primary signin-advice-button">Create Your
                    Account</a>
            </div>

        </div>
    </div>

    <jsp:include page="/bitground/include/footer.jsp" flush="true"></jsp:include>

    <script src="/bitground/js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/main.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>