<!doctype html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>bitground</title>
    <meta name="description" content="">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

    <link rel="manifest" href="/bitground/site.webmanifest">
    <link rel="apple-touch-icon" href="/bitground/icon.png">
    <link rel="shortcut icon" href="/bitground/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/bitground/css/base.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/common.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/board.css">
</head>

<body>
    <jsp:include page="/bitground/include/header.jsp" flush="true"></jsp:include>

    <div class="container">
        <div class="wrapper">
            <h2 class="visuallyhidden">Main Contents</h2>

            <div class="contents">
                <div class="c-page-header">
                    <h3 class="c-page-title">Q&A (1:1)</h3>
                    <jsp:include page="/bitground/include/search.jsp" flush="true"></jsp:include>
                </div>
                <!-- // .c-page-header -->

                <div class="c-view write-mode">
                    <div class="c-view-header">
                        <p class="c-view-item-name">Title</p>
                        <div class="c-view-item-detail">
                            <input class="c-write-input" type="text" value="Subscriptions have turned into a booming business for app developers">
                        </div>
                    </div>
                    <div class="c-view-body">
                        <p class="c-view-item-name">Contents</p>
                        <div class="c-view-item-detail">
                            <textarea class="c-write-textarea">Subscriptions have turned into a booming business for app developers, accounting for $10.6 billion in consumer spend on the App Store in 2017, and poised to grow to $75.7 billion by 2022. But alongside this healthy growth, a number of scammers are now taking advantage of subscriptions in order to trick users into signing up for expensive and recurring plans. They do this by intentionally confusing users with their app’s design and flow, by making promises of “free trials” that convert after only a matter of days, and other misleading tactics.</textarea>
                        </div>
                    </div>
                </div>

                <div class="c-view-buttons">
                    <button class="c-button view-list-button">Write</button>
                    <button type="button" class="c-button secondary view-list-button">Cancle</button>
                </div>

                <jsp:include page="/bitground/include/banner.jsp" flush="true"></jsp:include>

            </div>
            <!-- // .contents -->

        </div>
    </div>

    <jsp:include page="/bitground/include/footer.jsp" flush="true"></jsp:include>

    <script src="/bitground/js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/main.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>