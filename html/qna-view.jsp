<!doctype html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>bitground</title>
    <meta name="description" content="">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

    <link rel="manifest" href="/bitground/site.webmanifest">
    <link rel="apple-touch-icon" href="/bitground/icon.png">
    <link rel="shortcut icon" href="/bitground/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/bitground/css/base.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/common.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/board.css">
</head>

<body>
    <jsp:include page="/bitground/include/header.jsp" flush="true"></jsp:include>

    <div class="container">
        <div class="wrapper">
            <h2 class="visuallyhidden">Main Contents</h2>

            <div class="contents">
                <div class="c-page-header">
                    <h3 class="c-page-title">Q&A (1:1)</h3>
                    <jsp:include page="/bitground/include/search.jsp" flush="true"></jsp:include>
                </div>
                <!-- // .c-page-header -->

                <div class="c-view">
                    <div class="c-view-header">
                        <p class="c-view-item-name">Title</p>
                        <div class="c-view-item-detail">
                            <div class="c-view-heading">
                                <h4 class="c-view-heading-value">It would be a pleasure to check out my work and follow
                                    my It would be a pleasure to check out my work and follow
                                    my</h4>
                                <p class="qna-answer-state">
                                    <!-- <span class="label-checking">CHEKING</span> -->
                                    <span class="label-comp">COMP.</span>
                                    <span class="qna-complate-time">2018-12-27</span>
                                </p>
                            </div>
                            <p class="c-view-meta">
                                <span class="c-view-meta-name">Date</span>
                                2018-12-25
                            </p>
                        </div>
                    </div>
                    <div class="c-view-body">
                        <p class="c-view-item-name">Question</p>
                        <div class="c-view-item-detail">
                            <p>Some tech companies might have a problem taking money from the Department of Defense,
                                but Amazon isn’t one of them, as CEO Jeff Bezos made clear today at the Wired25
                                conference. Just last week, Google pulled out of the running for the Pentagon’s $10
                                billion, 10-year JEDI cloud contract, but Bezos suggested that he was happy to take the
                                government’s money.</p>
                        </div>
                    </div>
                    <div class="c-view-body qna-answer">
                        <p class="c-view-item-name">Answer</p>
                        <div class="c-view-item-detail">
                            <p>Last summer Oracle also filed a protest and also complained that they believed the
                                government had set up the contract to favor Amazon, a charge spokesperson Heather Babb
                                denied. “The JEDI Cloud final RFP reflects the unique and critical needs of DOD,
                                employing the best practices of competitive pricing and security. No vendors have been
                                pre-selected,” she said last month.<br />
                                While competitors are clearly worried about Amazon, which has a substantial lead in the
                                cloud infrastructure market, the company itself has kept quiet on the deal until now.
                                Bezos set his company’s support in patriotic terms and one of leadership.</p>
                        </div>
                    </div>
                </div>

                <div class="c-view-buttons">
                    <a href="/bitground/html/qna.jsp" class="c-button view-list-button">List</a>

                    <!-- 관리자가 확인하지 않은 경우 수정가능 -->
                    <a href="/bitground/html/qna-write.jsp" class="c-button line view-list-button">Modify</a>
                </div>

                <jsp:include page="/bitground/include/banner.jsp" flush="true"></jsp:include>

            </div>
            <!-- // .contents -->

        </div>
    </div>

    <jsp:include page="/bitground/include/footer.jsp" flush="true"></jsp:include>

    <script src="/bitground/js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/main.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>