<!doctype html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>bitground</title>
    <meta name="description" content="">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

    <link rel="manifest" href="/bitground/site.webmanifest">
    <link rel="apple-touch-icon" href="/bitground/icon.png">
    <link rel="shortcut icon" href="/bitground/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/bitground/css/base.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/common.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/member.css">
</head>

<body>
    <jsp:include page="/bitground/include/header.jsp" flush="true"></jsp:include>

    <div class="container">
        <div class="wrapper">
            <h2 class="visuallyhidden">Main Contents</h2>

            <div class="contents contents-member">
                <h3 class="c-page-title">Change Password</h3>

                <!-- Name -->
                <p class="myinfo-item">
                    <span class="sprite-icon avatar"></span>
                    Steve Jobs
                    <span class="myinfo-item-email">(neo_jobs@alksjdflkasjdlfkjas.com)</span>
                </p>

                <form action="#">
                    <fieldset class="change-password-form">

                        <!-- validation error났을 때 "invalid" 클래스명 추가 -->
                        <!-- Original password -->
                        <p class="input-field invalid">
                            <label for="pwOriginal" class="input-field-label">
                                <span class="sprite-icon password"></span>
                                Original password
                            </label>
                            <span class="input-field-input">
                                <input type="password" id="pwOriginal" placeholder="Enter your original password"
                                       autocomplete="current-password">
                            </span>
                            <strong class="input-field-message">The password you entered does not match.</strong>
                        </p>

                        <!-- New password -->
                        <p class="input-field invalid">
                            <label for="pwNew" class="input-field-label">
                                <span class="sprite-icon password"></span>
                                New password
                            </label>
                            <span class="input-field-input">
                                <input type="password" id="pwNew" placeholder="Enter your new password" autocomplete="new-password">
                            </span>
                        </p>

                        <!-- Confirm password -->
                        <p class="input-field invalid">
                            <label for="pwRe" class="input-field-label">
                                <span class="sprite-icon password"></span>
                                Confirm password
                            </label>
                            <span class="input-field-input">
                                <input type="password" id="pwRe" placeholder="Enter your new password again"
                                       autocomplete="new-password">
                            </span>
                            <strong class="input-field-message">The password you entered does not match.</strong>
                        </p>
                        <p class="password-guide">Your password must be least 8 characters long, but it is HIGHLY
                            recommended that you choose a random, alphanumeric password of at least 32 characters.</p>

                        <!-- 6-digit code -->
                        <p class="input-field invalid digit-code">
                            <label for="digitCode" class="input-field-label">
                                <span class="sprite-icon digit"></span>
                                6-digit code
                            </label>
                            <span class="input-field-input">
                                <input type="number" id="digitCode" placeholder="Enter Two-Factor Authentication code"
                                       autocomplete="new-password">
                            </span>
                            <strong class="input-field-message">The 6-digit Two-Factor Authentication code you entered
                                does not match.</strong>
                        </p>

                        <div class="button-wrap">
                            <button class="c-button medium change-password-confirm-button">Confirm</button>
                            <button type="button" class="c-button secondary medium change-password-cancel-button">Cancel</button>
                        </div>
                    </fieldset>
                </form>

            </div>

        </div>
    </div>

    <jsp:include page="/bitground/include/footer.jsp" flush="true"></jsp:include>

    <script src="/bitground/js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/main.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>