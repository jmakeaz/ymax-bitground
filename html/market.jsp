<!doctype html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>bitground</title>
    <meta name="description" content="">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

    <link rel="manifest" href="/bitground/site.webmanifest">
    <link rel="apple-touch-icon" href="/bitground/icon.png">
    <link rel="shortcut icon" href="/bitground/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/bitground/css/base.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/common.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/market.css">
</head>

<body>
    <jsp:include page="/bitground/include/header.jsp" flush="true"></jsp:include>

    <div class="container">
        <div class="wrapper">
            <h2 class="visuallyhidden">Main Contents</h2>

            <div class="contents">
                <div class="c-page-header">
                    <h3 class="c-page-title">Market</h3>
                </div>
                <!-- // .c-page-header -->

                <div class="transaction-daily">
                    <h3 class="transaction-title">Transaction Amount</h3>
                    <div class="transaction-daily-line">
                        <h4 class="transaction-daily-time">Today</h4>
                        <ul class="transaction-daily-list">
                            <li>
                                150,000,145,740.0000
                                <em class="transaction-daily-label">PHP</em>
                            </li>
                            <li>
                                350,025,000,000.0000
                                <em class="transaction-daily-label">BTC</em>
                            </li>
                            <li>
                                214,000,140,000.0000
                                <em class="transaction-daily-label">ETH</em>
                            </li>
                        </ul>
                    </div>
                    <div class="transaction-daily-line">
                        <h4 class="transaction-daily-time">Yesterday</h4>
                        <ul class="transaction-daily-list">
                            <li>
                                <span>170,150,155,740.1586</span>
                                <em class="transaction-daily-label">PHP</em>
                            </li>
                            <li>
                                <span>240,245,000,218.0000</span>
                                <em class="transaction-daily-label">BTC</em>
                            </li>
                            <li>
                                <span>184,000,430,000.0000</span>
                                <em class="transaction-daily-label">ETH</em>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- // .transaction-daily -->

                <div class="market-wrap">
                    <dl class="market-section">
                        <dt class="market-section-title">
                            BITGROUND<br />
                            Global Market
                        </dt>
                        <dd class="market-section-content">
                            <strong class="market-section-title2">Global Market</strong>
                            <p class="market-section-text">
                                You can find information about the coins that are deployed for each market.<br />
                                You can also check the status of transactions by coin.
                            </p>
                            <p class="market-section-text t-secondary">
                                Currently, market capabilities are only available<br />
                                through Mobile APP.<br />
                                <strong>
                                    Please download and install an open<br />
                                    Mobile APP.
                                </strong>
                            </p>
                        </dd>
                    </dl>
                    <dl class="market-section">
                        <dt class="market-section-title">
                            Mobile App<br />
                            Open
                        </dt>
                        <dd class="market-section-content">
                            <div class="market-section-buttons">
                                <a href="#" class="app-open-button">
                                    <img src="/bitground/img/google-play.png" alt="Get it on Google Play">
                                </a>
                                <a href="#" class="app-open-button">
                                    <img src="/bitground/img/app-store.png" alt="Download on the App Store">
                                </a>
                            </div>
                        </dd>
                    </dl>
                </div>

            </div>
            <!-- // .contents -->

        </div>
    </div>

    <jsp:include page="/bitground/include/footer.jsp" flush="true"></jsp:include>

    <script src="/bitground/js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/main.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>