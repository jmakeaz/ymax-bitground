<!doctype html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>bitground</title>
    <meta name="description" content="">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

    <link rel="manifest" href="/bitground/site.webmanifest">
    <link rel="apple-touch-icon" href="/bitground/icon.png">
    <link rel="shortcut icon" href="/bitground/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/bitground/css/base.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/common.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/member.css">
</head>

<body>
    <jsp:include page="/bitground/include/header.jsp" flush="true"></jsp:include>

    <div class="container">
        <div class="wrapper">
            <h2 class="visuallyhidden">Main Contents</h2>

            <div class="contents contents-member">
                <h3 class="c-page-title">My page</h3>
                <h4 class="c-page-sub-title">Profile</h4>

                <!-- Name -->
                <p class="myinfo-item">
                    <span class="sprite-icon avatar-black"></span>
                    Steve Jobs
                </p>

                <!-- ID -->
                <p class="myinfo-item">
                    <span class="sprite-icon email-black"></span>
                    neo_jobs@alksjdflkasjdlfkjas.com
                </p>

                <!-- log IP -->
                <p class="myinfo-signin">
                    <span class="myinfo-signin-label">Last Sign in IP</span>
                    192.256.983 (2018-12-25 23:01:58)
                </p>

                <h4 class="c-page-sub-title2">Authentication</h4>
                <p class="c-pape-title-desc">We strongly suggest you to register the Two-Factor Authentication for
                    enhanced security control to protect your digital assets and personal information.</p>

                <!-- Email Verification -->
                <dl class="myauth-item verified">
                    <dt class="myauth-item-label">
                        <span class="sprite-icon verify-code-black"></span>
                        <span>Email Verification</span>
                    </dt>
                    <dd class="myauth-item-desc">
                        <span class="auth-ready">In ready in web</span>
                        <span class="auth-verified">Verified</span>
                        <span class="auth-retry">Verify again</span>
                    </dd>
                    <dd class="myauth-item-message">
                        <span class="myauth-item-help">Thanks for registering with us. Your email account has been
                            successfully verified.</span>
                    </dd>
                </dl>

                <!-- Phone Number -->
                <dl class="myauth-item">
                    <dt class="myauth-item-label">
                        <span class="sprite-icon phone"></span>
                        <span>Phone Number</span>
                    </dt>
                    <dd class="myauth-item-desc">
                        <span class="auth-ready">In ready in web</span>
                        <span class="auth-verified">Verified</span>
                        <span class="auth-retry">Verify again</span>
                    </dd>
                    <dd class="myauth-item-message">
                        <span class="myauth-item-help">Providing a phone number will make your account more secure.<br />
                            You can quickly reset your password and your account operations will be notified.</span>
                    </dd>
                </dl>

                <!-- Verify Account -->
                <dl class="myauth-item not-verified">
                    <dt class="myauth-item-label">
                        <span class="sprite-icon document"></span>
                        <span>Verify Account</span>
                    </dt>
                    <dd class="myauth-item-desc">
                        <span class="auth-ready">In ready in web</span>
                        <span class="auth-verified">Verified</span>
                        <span class="auth-retry">Verify again</span>
                    </dd>
                    <dd class="myauth-item-message">
                        <span class="myauth-item-help">Help keep the bad guys out of your account by using both your
                            password and your phone.</span>
                        <p class="c-invalid-message">Please proceed with the verify process again.</p>
                    </dd>
                </dl>

                <!-- Two-Factor Authentication -->
                <dl class="myauth-item">
                    <dt class="myauth-item-label">
                        <span class="sprite-icon digit-black"></span>
                        <span>Two-Factor Authentication</span>
                    </dt>
                    <dd class="myauth-item-desc">
                        <span class="auth-ready">In ready in web</span>
                        <span class="auth-verified">Verified</span>
                        <span class="auth-retry">Verify again</span>
                    </dd>
                    <dd class="myauth-item-message">
                        <span class="myauth-item-help">Two-Factor Authentication provides another layer of security to
                            your account.</span>
                    </dd>
                </dl>

                <!-- Password -->
                <dl class="myauth-item not-verified">
                    <dt class="myauth-item-label">
                        <span class="sprite-icon password-black"></span>
                        <span>Password</span>
                    </dt>
                    <dd class="myauth-item-desc">
                        <a href="/bitground/html/mypage-password.jsp" class="c-button myauth-password-button">Change
                            Password</a>
                    </dd>
                    <dd class="myauth-item-message">
                        <span class="myauth-item-help">This password is required for sign in, please remember it.</span>
                        <p class="c-invalid-message">Please try after you have received the Two-Factor Authentication
                            provides.</p>
                    </dd>
                </dl>





            </div>

        </div>
    </div>

    <jsp:include page="/bitground/include/footer.jsp" flush="true"></jsp:include>

    <script src="/bitground/js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/main.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>