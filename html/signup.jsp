<!doctype html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>bitground</title>
    <meta name="description" content="">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

    <link rel="manifest" href="/bitground/site.webmanifest">
    <link rel="apple-touch-icon" href="/bitground/icon.png">
    <link rel="shortcut icon" href="/bitground/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/bitground/css/base.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/common.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/member.css">
</head>

<body>
    <jsp:include page="/bitground/include/header.jsp" flush="true"></jsp:include>

    <div class="container">
        <div class="wrapper">
            <h2 class="visuallyhidden">Main Contents</h2>

            <div class="contents contents-member">
                <h3 class="c-page-title">Sign Up</h3>

                <form action="#">
                    <fieldset>
                        <p class="input-field">
                            <label for="userName" class="input-field-label">
                                <span class="sprite-icon avatar"></span>
                                User Name
                                <span class="c-required">*<span class="visuallyhidden">required</span></span>
                            </label>
                            <span class="input-field-input">
                                <input type="text" id="userName" value="Steve Jobs" placeholder="Enter your name">
                            </span>
                            <!-- <strong class="input-field-message">This email is already in use.</strong> -->
                        </p>
                        <p class="input-field">
                            <label for="sex" class="input-field-label">
                                <span class="sprite-icon gender"></span>
                                Gender
                                <span class="c-required">*<span class="visuallyhidden">required</span></span>
                            </label>
                            <span class="input-field-input">
                                <span class="c-select">
                                    <select id="sex">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </span>
                            </span>
                            <!-- <strong class="input-field-message">This email is already in use.</strong> -->
                        </p>

                        <!-- validation error났을 때 "invalid" 클래스명 추가 -->
                        <p class="input-field invalid">
                            <label for="email" class="input-field-label">
                                <span class="sprite-icon email"></span>
                                Email
                                <span class="c-required">*<span class="visuallyhidden">required</span></span>
                            </label>
                            <span class="input-field-input">
                                <input type="email" id="email" value="neo@gmail.com" placeholder="Enter your email address"
                                       autocomplete="username">
                            </span>
                            <strong class="input-field-message">This email is already in use.</strong>
                        </p>
                        <p class="input-field">
                            <label for="password" class="input-field-label">
                                <span class="sprite-icon password"></span>
                                Password
                                <span class="c-required">*<span class="visuallyhidden">required</span></span>
                            </label>
                            <span class="input-field-input">
                                <input type="password" id="password" value="111111111" placeholder="Enter your password"
                                       autocomplete="new-password">
                            </span>
                            <!-- <strong class="input-field-message">This email is already in use.</strong> -->
                        </p>
                        <p class="input-field invalid">
                            <label for="new-password" class="input-field-label">
                                <span class="sprite-icon password"></span>
                                Confirm Password
                                <span class="c-required">*<span class="visuallyhidden">required</span></span>
                            </label>
                            <span class="input-field-input">
                                <input type="password" id="new-password" value="111111111" placeholder="Enter your password again"
                                       autocomplete="new-password">
                            </span>
                            <strong class="input-field-message">The password you entered does not match.</strong>
                        </p>
                        <div class="password-guide">
                            <p class="password-guide-title">Password selected must meet the following criteria:</p>
                            <ul class="password-guide-list">
                                <li>- Minimum of 8 characters.</li>
                                <li>- Contain a combination of upper and lower- case letters Number(s).</li>
                                <li>- Special character(s)</li>
                                <li>- Example: Bit12#$% )</li>
                            </ul>
                        </div>

                        <!-- validation error났을 때 "invalid" 클래스명 추가 -->
                        <p class="c-checkbox signup-agree invalid">
                            <input type="checkbox" id="agreeThat" class="visuallyhidden">
                            <label for="agreeThat" class="c-checkbox-label">
                                I agree to bitground's
                                <a href="#">Terms of Service</a> and
                                <a href="#">Privacy Policy</a>
                                <span class="c-required">*<span class="visuallyhidden">required</span></span>
                            </label>
                        </p>
                        <p class="c-invalid-message">Please agree to the Terms of Service and the privacy policy.</p>

                        <div class="button-wrap">
                            <button class="c-button medium signup-register">Register</button>
                        </div>
                    </fieldset>
                </form>
            </div>

        </div>
    </div>

    <jsp:include page="/bitground/include/footer.jsp" flush="true"></jsp:include>

    <script src="/bitground/js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/main.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>