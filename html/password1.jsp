<!doctype html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>bitground</title>
    <meta name="description" content="">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

    <link rel="manifest" href="/bitground/site.webmanifest">
    <link rel="apple-touch-icon" href="/bitground/icon.png">
    <link rel="shortcut icon" href="/bitground/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/bitground/css/base.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/common.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/member.css">
</head>

<body>
    <jsp:include page="/bitground/include/header.jsp" flush="true"></jsp:include>

    <div class="container">
        <div class="wrapper">
            <h2 class="visuallyhidden">Main Contents</h2>

            <div class="contents contents-member">
                <h3 class="c-page-title">Forgot Password</h3>

                <form action="#">
                    <fieldset>
                        <p class="input-field">
                            <label for="currentEmail" class="input-field-label">
                                <span class="sprite-icon verify-code"></span>
                                Sign in Email
                            </label>
                            <span class="input-field-input">
                                <input type="email" id="currentEmail" value="" placeholder="Enter your email address">
                            </span>
                        </p>

                        <div class="button-wrap">
                            <button class="c-button medium forgot-password-button">Reset Sign in Password</button>
                            <a href="/bitground/html/signin.jsp" class="c-button medium secondary forgot-password-cancel-button">Cancel</a>
                        </div>
                    </fieldset>
                </form>
            </div>

        </div>
    </div>

    <jsp:include page="/bitground/include/footer.jsp" flush="true"></jsp:include>

    <script src="/bitground/js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/main.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>