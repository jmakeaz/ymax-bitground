<!doctype html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>bitground</title>
    <meta name="description" content="">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

    <link rel="manifest" href="/bitground/site.webmanifest">
    <link rel="apple-touch-icon" href="/bitground/icon.png">
    <link rel="shortcut icon" href="/bitground/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/bitground/css/base.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/common.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/board.css">
</head>

<body>
    <jsp:include page="/bitground/include/header.jsp" flush="true"></jsp:include>

    <div class="container">
        <div class="wrapper">
            <h2 class="visuallyhidden">Main Contents</h2>

            <div class="contents">
                <div class="c-page-header">
                    <h3 class="c-page-title">Notice</h3>
                    <jsp:include page="/bitground/include/search.jsp" flush="true"></jsp:include>
                </div>
                <!-- // .c-page-header -->

                <div class="c-board notice-board">
                    <table>
                        <thead>
                            <tr>
                                <th class="t-left">No.</th>
                                <th class="t-left">Title</th>
                                <th class="t-right">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="notice-writing">
                                <td class="t-left">Notice</td>
                                <td class="t-left">
                                    <a href="/bitground/html/notice-view.jsp" class="title ellipsis">Wow awesome
                                        design. really like the
                                        illustrations.</a>
                                </td>
                                <td class="t-right">2018-12-25</td>
                            </tr>
                            <tr class="notice-writing">
                                <td class="t-left">Notice</td>
                                <td class="t-left">
                                    <a href="/bitground/html/notice-view.jsp" class="title ellipsis">It would be a
                                        pleasure to check out my work and
                                        follow my works. It's impressive, all business.dovery co...</a>
                                </td>
                                <td class="t-right">2018-12-25</td>
                            </tr>
                            <tr>
                                <td class="t-left">34,215</td>
                                <td class="t-left">
                                    <a href="/bitground/html/notice-view.jsp" class="title ellipsis">Good day to
                                        everyone! </a>
                                </td>
                                <td class="t-right">2018-12-25</td>
                            </tr>
                            <tr>
                                <td class="t-left">185</td>
                                <td class="t-left">
                                    <a href="/bitground/html/notice-view.jsp" class="title ellipsis">Here is our
                                        concept for banking site.</a>
                                </td>
                                <td class="t-right">2018-12-25</td>
                            </tr>
                            <tr>
                                <td class="t-left">89</td>
                                <td class="t-left">
                                    <a href="/bitground/html/notice-view.jsp" class="title ellipsis">Stay tuned and
                                        have a nice day!)</a>
                                </td>
                                <td class="t-right">2018-12-25</td>
                            </tr>
                            <tr>
                                <td class="t-left">45</td>
                                <td class="t-left">
                                    <a href="/bitground/html/notice-view.jsp" class="title ellipsis">Check the real
                                        pixels and press 'L' if you like!
                                        :3</a>
                                </td>
                                <td class="t-right">2018-12-25</td>
                            </tr>
                            <tr>
                                <td class="t-left">10</td>
                                <td class="t-left">
                                    <a href="/bitground/html/notice-view.jsp" class="title ellipsis">Very nice and
                                        amazing. </a>
                                </td>
                                <td class="t-right">2018-12-25</td>
                            </tr>
                            <tr>
                                <td class="t-left">9</td>
                                <td class="t-left">
                                    <a href="/bitground/html/notice-view.jsp" class="title ellipsis">It would be a
                                        pleasure to check out my work and
                                        follow my works.</a>
                                </td>
                                <td class="t-right">2018-12-25</td>
                            </tr>
                            <tr>
                                <td class="t-left">8</td>
                                <td class="t-left">
                                    <a href="/bitground/html/notice-view.jsp" class="title ellipsis">It's impressive,
                                        all very competently, excellent
                                        job!</a>
                                </td>
                                <td class="t-right">2018-12-25</td>
                            </tr>
                            <tr>
                                <td class="t-left">7</td>
                                <td class="t-left">
                                    <a href="/bitground/html/notice-view.jsp" class="title ellipsis">Stay tuned and
                                        have a nice day!)</a>
                                </td>
                                <td class="t-right">2018-12-25</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="c-pagination-wrap" aria-label="pagination" role="navigation">
                    <ul class="c-pagination">
                        <li class="c-pagination-item previous-group">
                            <a class="c-pagination-link-arrow" href="#">
                                <span class="visuallyhidden">Previous set of pages</span>
                                <span class="sprite-direction arrow-double-to-left"></span>
                            </a>
                        </li>
                        <li class="c-pagination-item previous-page">
                            <a class="c-pagination-link-arrow" href="#">
                                <span class="visuallyhidden">Previous page</span>
                                <span class="sprite-direction arrow-to-left"></span>
                            </a>
                        </li>
                        <li class="c-pagination-item">
                            <a class="c-pagination-link" href="#">
                                <span class="visuallyhidden">page </span>1
                            </a>
                        </li>
                        <li class="c-pagination-item">
                            <a class="c-pagination-link current" href="#">
                                <span class="visuallyhidden">page </span>2
                            </a>
                        </li>
                        <li class="c-pagination-item">
                            <a class="c-pagination-link" href="#">
                                <span class="visuallyhidden">page</span>3
                            </a>
                        </li>
                        <li class="c-pagination-item">
                            <a class="c-pagination-link" href="#">
                                <span class="visuallyhidden">page </span>4
                            </a>
                        </li>
                        <li class="c-pagination-item">
                            <a class="c-pagination-link" href="#">
                                <span class="visuallyhidden">page </span>5
                            </a>
                        </li>
                        <li class="c-pagination-item">
                            <a class="c-pagination-link" href="#">
                                <span class="visuallyhidden">page </span>6
                            </a>
                        </li>
                        <li class="c-pagination-item">
                            <a class="c-pagination-link" href="#">
                                <span class="visuallyhidden">page </span>7
                            </a>
                        </li>
                        <li class="c-pagination-item">
                            <a class="c-pagination-link" href="#">
                                <span class="visuallyhidden">page </span>8
                            </a>
                        </li>
                        <li class="c-pagination-item">
                            <a class="c-pagination-link" href="#">
                                <span class="visuallyhidden">page </span>9
                            </a>
                        </li>
                        <li class="c-pagination-item">
                            <a class="c-pagination-link" href="#">
                                <span class="visuallyhidden">page </span>10
                            </a>
                        </li>
                        <li class="c-pagination-item next-page">
                            <a class="c-pagination-link-arrow" href="#">
                                <span class="visuallyhidden">Next page</span>
                                <span class="sprite-direction arrow-to-right"></span>
                            </a>
                        </li>
                        <li class="c-pagination-item next-group">
                            <a class="c-pagination-link-arrow disabled" href="#">
                                <span class="visuallyhidden">Next set of pages</span>
                                <span class="sprite-direction arrow-double-to-right"></span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- // .pagination-wrap -->

                <jsp:include page="/bitground/include/banner.jsp" flush="true"></jsp:include>

            </div>
            <!-- // .contents -->

        </div>
    </div>

    <jsp:include page="/bitground/include/footer.jsp" flush="true"></jsp:include>

    <script src="/bitground/js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/main.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>