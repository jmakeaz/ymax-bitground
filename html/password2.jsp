<!doctype html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>bitground</title>
    <meta name="description" content="">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

    <link rel="manifest" href="/bitground/site.webmanifest">
    <link rel="apple-touch-icon" href="/bitground/icon.png">
    <link rel="shortcut icon" href="/bitground/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/bitground/css/base.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/common.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/member.css">
</head>

<body>
    <jsp:include page="/bitground/include/header.jsp" flush="true"></jsp:include>

    <div class="container">
        <div class="wrapper">
            <h2 class="visuallyhidden">Main Contents</h2>

            <div class="contents contents-member">
                <h3 class="c-page-title">Forgot Password</h3>

                <form action="#">
                    <fieldset>
                        <div class="top-message-line">
                            <p class="c-validation-message">You have been authenticated to change your password via
                                your email.</p>

                            <!-- 패스워드가 기존과 동일하거나 패스워드 규칙에 안맞는 경우 -->
                            <!-- <p class="c-validation-message invalid">New Password Invalid Password, Confirm Password invalid password!</p> -->

                            <!-- 6자리 코드가 안맞는 경우 -->
                            <!-- <p class="c-validation-message invalid">The 6-digit code you entered does not match.</p> -->
                        </div>

                        <p class="input-field">
                            <label for="newPassword" class="input-field-label">
                                <span class="sprite-icon password"></span>
                                New Password
                            </label>
                            <span class="input-field-input">
                                <input type="password" id="newPassword" value="" placeholder="Please set your sign in password">
                            </span>
                        </p>
                        <p class="input-field-guide">Set a complex password with at least 8 characters, with the
                            combinations
                            of uppercase and
                            lowercase characters, numbers and special characters.</p>

                        <p class="input-field">
                            <label for="code6" class="input-field-label">
                                <span class="sprite-icon verify-code"></span>
                                Code
                            </label>
                            <span class="input-field-input">
                                <input type="number" id="code6" value="" placeholder="Enter code by your email">
                            </span>
                        </p>

                        <div class="button-wrap">
                            <button class="c-button medium forgot-password-button">Reset Sign in Password</button>
                        </div>
                    </fieldset>
                </form>
            </div>

        </div>
    </div>

    <jsp:include page="/bitground/include/footer.jsp" flush="true"></jsp:include>

    <script src="/bitground/js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/main.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>