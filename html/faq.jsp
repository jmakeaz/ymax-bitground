<!doctype html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>bitground</title>
    <meta name="description" content="">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

    <link rel="manifest" href="/bitground/site.webmanifest">
    <link rel="apple-touch-icon" href="/bitground/icon.png">
    <link rel="shortcut icon" href="/bitground/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/bitground/css/base.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/common.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/board.css">
</head>

<body>
    <jsp:include page="/bitground/include/header.jsp" flush="true"></jsp:include>

    <div class="container">
        <div class="wrapper">
            <h2 class="visuallyhidden">Main Contents</h2>

            <div class="contents">
                <div class="c-page-header">
                    <h3 class="c-page-title">FAQ</h3>
                    <jsp:include page="/bitground/include/search.jsp" flush="true"></jsp:include>
                </div>
                <!-- // .c-page-header -->

                <ul class="faq-tablist js-faq-tab" role="tablist">
                    <li class="faq-tab active" data-controls="tab-panel-all" role="tab"><span class="faq-tab-value">All</span></li>
                    <li class="faq-tab" data-controls="tab-panel-market" role="tab"><span class="faq-tab-value">Market</span></li>
                    <li class="faq-tab" data-controls="tab-panel-exchange" role="tab"><span class="faq-tab-value">Exchange</span></li>
                    <li class="faq-tab" data-controls="tab-panel-trading" role="tab"><span class="faq-tab-value">Trading</span></li>
                    <li class="faq-tab" data-controls="tab-panel-deposit" role="tab"><span class="faq-tab-value">Deposit</span></li>
                    <li class="faq-tab" data-controls="tab-panel-draw" role="tab"><span class="faq-tab-value">With Draw</span></li>
                    <li class="faq-tab" data-controls="tab-panel-etc" role="tab"><span class="faq-tab-value">Etc</span></li>
                </ul>

                <div class="faq-tab-panel tab-panel-all" role="tabpanel" style="display: block;">
                    <div class="faq-header">
                        <span class="faq-header-qna">Q/A</span>
                        <span class="faq-header-title">Title</span>
                    </div>
                    <ul class="faq-list js-faq-list">
                        <li class="faq-item">
                            <div class="faq-item-question">
                                <strong class="faq-item-label">Q<span class="faq-item-label-word">uestion</span></strong>

                                <!-- 질문 -->
                                <div class="faq-item-content">
                                    Wow awesome design. really like the illustrations.
                                </div>
                            </div>
                            <div class="faq-item-answer">
                                <strong class="faq-item-label">Answer</strong>

                                <!-- 대답 -->
                                <div class="faq-item-content">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis debitis distinctio,
                                    explicabo, facere ad aperiam similique doloribus reiciendis nam, corporis veniam
                                    sequi dolorum. Eveniet accusantium officia earum tenetur? Harum, necessitatibus.
                                </div>
                            </div>
                        </li>
                        <li class="faq-item">
                            <div class="faq-item-question">
                                <strong class="faq-item-label">Q<span class="faq-item-label-word">uestion</span></strong>

                                <!-- 질문 -->
                                <div class="faq-item-content">
                                    It would be a pleasure to check out my work and follow my works. It's impressive,
                                    all very co...
                                </div>
                            </div>
                            <div class="faq-item-answer">
                                <strong class="faq-item-label">Answer</strong>

                                <!-- 대답 -->
                                <div class="faq-item-content">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis debitis distinctio,
                                    explicabo, facere ad aperiam similique doloribus reiciendis nam, corporis veniam
                                    sequi dolorum. Eveniet accusantium officia earum tenetur? Harum, necessitatibus.
                                </div>
                            </div>
                        </li>
                        <li class="faq-item">
                            <div class="faq-item-question">
                                <strong class="faq-item-label">Q<span class="faq-item-label-word">uestion</span></strong>

                                <!-- 질문 -->
                                <div class="faq-item-content">
                                    Good day to everyone!
                                </div>
                            </div>
                            <div class="faq-item-answer">
                                <strong class="faq-item-label">Answer</strong>

                                <!-- 대답 -->
                                <div class="faq-item-content">
                                    Today, the majority of the Top Grossing apps on Apple’s App Store are streaming
                                    services, dating sites, entertainment apps or games. But when you get past the
                                    market leaders — apps like Fortnite, Netflix, Pandora, Tinder, Hulu, etc. — and
                                    down into the top hundreds on the Top Grossing chart, another type of app appears:
                                    Utilities.
                                    How are apps like QR code readers, document scanners, translators and weather apps
                                    raking in so much money? Especially when some of their utilitarian functions can be
                                    found elsewhere for much less, or even for free?
                                    This raises the question as to whether some app developers are trying to scam App
                                    Store users by way of subscriptions.
                                    We’ve found that does appear to be true, in many cases.
                                    After reading through the critical reviews across the top money-making
                                    utilities, you’ll find customers complaining that the apps are too aggressive in
                                    pushing subscriptions (e.g. via constant prompts), offer little functionality
                                    without upamong other things.
                                </div>
                            </div>
                        </li>
                        <li class="faq-item">
                            <div class="faq-item-question">
                                <strong class="faq-item-label">Q<span class="faq-item-label-word">uestion</span></strong>

                                <!-- 질문 -->
                                <div class="faq-item-content">
                                    Here is our concept for banking site.
                                </div>
                            </div>
                            <div class="faq-item-answer">
                                <strong class="faq-item-label">Answer</strong>

                                <!-- 대답 -->
                                <div class="faq-item-content">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis debitis distinctio,
                                    explicabo, facere ad aperiam similique doloribus reiciendis nam, corporis veniam
                                    sequi dolorum. Eveniet accusantium officia earum tenetur? Harum, necessitatibus.
                                </div>
                            </div>
                        </li>
                        <li class="faq-item">
                            <div class="faq-item-question">
                                <strong class="faq-item-label">Q<span class="faq-item-label-word">uestion</span></strong>

                                <!-- 질문 -->
                                <div class="faq-item-content">
                                    Stay tuned and have a nice day!)
                                </div>
                            </div>
                            <div class="faq-item-answer">
                                <strong class="faq-item-label">Answer</strong>

                                <!-- 대답 -->
                                <div class="faq-item-content">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis debitis distinctio,
                                    explicabo, facere ad aperiam similique doloribus reiciendis nam, corporis veniam
                                    sequi dolorum. Eveniet accusantium officia earum tenetur? Harum, necessitatibus.
                                </div>
                            </div>
                        </li>
                        <li class="faq-item">
                            <div class="faq-item-question">
                                <strong class="faq-item-label">Q<span class="faq-item-label-word">uestion</span></strong>

                                <!-- 질문 -->
                                <div class="faq-item-content">
                                    Check the real pixels and press 'L' if you like! :3
                                </div>
                            </div>
                            <div class="faq-item-answer">
                                <strong class="faq-item-label">Answer</strong>

                                <!-- 대답 -->
                                <div class="faq-item-content">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis debitis distinctio,
                                    explicabo, facere ad aperiam similique doloribus reiciendis nam, corporis veniam
                                    sequi dolorum. Eveniet accusantium officia earum tenetur? Harum, necessitatibus.
                                </div>
                            </div>
                        </li>
                        <li class="faq-item">
                            <div class="faq-item-question">
                                <strong class="faq-item-label">Q<span class="faq-item-label-word">uestion</span></strong>

                                <!-- 질문 -->
                                <div class="faq-item-content">
                                    Very nice and amazing.
                                </div>
                            </div>
                            <div class="faq-item-answer">
                                <strong class="faq-item-label">Answer</strong>

                                <!-- 대답 -->
                                <div class="faq-item-content">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis debitis distinctio,
                                    explicabo, facere ad aperiam similique doloribus reiciendis nam, corporis veniam
                                    sequi dolorum. Eveniet accusantium officia earum tenetur? Harum, necessitatibus.
                                </div>
                            </div>
                        </li>
                        <li class="faq-item">
                            <div class="faq-item-question">
                                <strong class="faq-item-label">Q<span class="faq-item-label-word">uestion</span></strong>

                                <!-- 질문 -->
                                <div class="faq-item-content">
                                    It would be a pleasure to check out my work and follow my works.
                                </div>
                            </div>
                            <div class="faq-item-answer">
                                <strong class="faq-item-label">Answer</strong>

                                <!-- 대답 -->
                                <div class="faq-item-content">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis debitis distinctio,
                                    explicabo, facere ad aperiam similique doloribus reiciendis nam, corporis veniam
                                    sequi dolorum. Eveniet accusantium officia earum tenetur? Harum, necessitatibus.
                                </div>
                            </div>
                        </li>
                        <li class="faq-item">
                            <div class="faq-item-question">
                                <strong class="faq-item-label">Q<span class="faq-item-label-word">uestion</span></strong>

                                <!-- 질문 -->
                                <div class="faq-item-content">
                                    It's impressive, all very competently, excellent job!
                                </div>
                            </div>
                            <div class="faq-item-answer">
                                <strong class="faq-item-label">Answer</strong>

                                <!-- 대답 -->
                                <div class="faq-item-content">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis debitis distinctio,
                                    explicabo, facere ad aperiam similique doloribus reiciendis nam, corporis veniam
                                    sequi dolorum. Eveniet accusantium officia earum tenetur? Harum, necessitatibus.
                                </div>
                            </div>
                        </li>
                        <li class="faq-item">
                            <div class="faq-item-question">
                                <strong class="faq-item-label">Q<span class="faq-item-label-word">uestion</span></strong>

                                <!-- 질문 -->
                                <div class="faq-item-content">
                                    Stay tuned and have a nice day!)
                                </div>
                            </div>
                            <div class="faq-item-answer">
                                <strong class="faq-item-label">Answer</strong>

                                <!-- 대답 -->
                                <div class="faq-item-content">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis debitis distinctio,
                                    explicabo, facere ad aperiam similique doloribus reiciendis nam, corporis veniam
                                    sequi dolorum. Eveniet accusantium officia earum tenetur? Harum, necessitatibus.
                                </div>
                            </div>
                        </li>
                    </ul>

                    <div class="c-pagination-wrap" aria-label="pagination" role="navigation">
                        <ul class="c-pagination">
                            <li class="c-pagination-item previous-group">
                                <a class="c-pagination-link-arrow" href="#">
                                    <span class="visuallyhidden">Previous set of pages</span>
                                    <span class="sprite-direction arrow-double-to-left"></span>
                                </a>
                            </li>
                            <li class="c-pagination-item previous-page">
                                <a class="c-pagination-link-arrow" href="#">
                                    <span class="visuallyhidden">Previous page</span>
                                    <span class="sprite-direction arrow-to-left"></span>
                                </a>
                            </li>
                            <li class="c-pagination-item">
                                <a class="c-pagination-link" href="#">
                                    <span class="visuallyhidden">page </span>1
                                </a>
                            </li>
                            <li class="c-pagination-item">
                                <a class="c-pagination-link current" href="#">
                                    <span class="visuallyhidden">page </span>2
                                </a>
                            </li>
                            <li class="c-pagination-item">
                                <a class="c-pagination-link" href="#">
                                    <span class="visuallyhidden">page</span>3
                                </a>
                            </li>
                            <li class="c-pagination-item">
                                <a class="c-pagination-link" href="#">
                                    <span class="visuallyhidden">page </span>4
                                </a>
                            </li>
                            <li class="c-pagination-item">
                                <a class="c-pagination-link" href="#">
                                    <span class="visuallyhidden">page </span>5
                                </a>
                            </li>
                            <li class="c-pagination-item">
                                <a class="c-pagination-link" href="#">
                                    <span class="visuallyhidden">page </span>6
                                </a>
                            </li>
                            <li class="c-pagination-item">
                                <a class="c-pagination-link" href="#">
                                    <span class="visuallyhidden">page </span>7
                                </a>
                            </li>
                            <li class="c-pagination-item">
                                <a class="c-pagination-link" href="#">
                                    <span class="visuallyhidden">page </span>8
                                </a>
                            </li>
                            <li class="c-pagination-item">
                                <a class="c-pagination-link" href="#">
                                    <span class="visuallyhidden">page </span>9
                                </a>
                            </li>
                            <li class="c-pagination-item">
                                <a class="c-pagination-link" href="#">
                                    <span class="visuallyhidden">page </span>10
                                </a>
                            </li>
                            <li class="c-pagination-item next-page">
                                <a class="c-pagination-link-arrow" href="#">
                                    <span class="visuallyhidden">Next page</span>
                                    <span class="sprite-direction arrow-to-right"></span>
                                </a>
                            </li>
                            <li class="c-pagination-item next-group">
                                <a class="c-pagination-link-arrow disabled" href="#">
                                    <span class="visuallyhidden">Next set of pages</span>
                                    <span class="sprite-direction arrow-double-to-right"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="faq-tab-panel tab-panel-market" role="tabpanel">
                    <div class="faq-header">
                        <span class="faq-header-qna">Q/A</span>
                        <span class="faq-header-title">Title</span>
                    </div>
                    <ul class="faq-list js-faq-list">
                        <li class="faq-item">
                            <div class="faq-item-question">
                                <strong class="faq-item-label">Q<span class="faq-item-label-word">uestion</span></strong>

                                <!-- 질문 -->
                                <div class="faq-item-content">
                                    Wow awesome design. really like the illustrations.
                                </div>
                            </div>
                            <div class="faq-item-answer">
                                <strong class="faq-item-label">Answer</strong>

                                <!-- 대답 -->
                                <div class="faq-item-content">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis debitis distinctio,
                                    explicabo, facere ad aperiam similique doloribus reiciendis nam, corporis veniam
                                    sequi dolorum. Eveniet accusantium officia earum tenetur? Harum, necessitatibus.
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="c-pagination-wrap" aria-label="pagination" role="navigation">
                        <ul class="c-pagination">
                            <li class="c-pagination-item">
                                <a class="c-pagination-link current" href="#">
                                    <span class="visuallyhidden">page </span>1
                                </a>
                            </li>
                            <li class="c-pagination-item">
                                <a class="c-pagination-link" href="#">
                                    <span class="visuallyhidden">page </span>2
                                </a>
                            </li>
                            <li class="c-pagination-item next-page">
                                <a class="c-pagination-link-arrow" href="#">
                                    <span class="visuallyhidden">Next page</span>
                                    <span class="sprite-direction arrow-to-right"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="faq-tab-panel tab-panel-exchange" role="tabpanel">
                    <div class="faq-header">
                        <span class="faq-header-qna">Q/A</span>
                        <span class="faq-header-title">Title</span>
                    </div>
                    <ul class="faq-list js-faq-list">
                        <li class="faq-item">
                            <div class="faq-item-question">
                                <strong class="faq-item-label">Q<span class="faq-item-label-word">uestion</span></strong>

                                <!-- 질문 -->
                                <div class="faq-item-content">
                                    Wow awesome design. really like the illustrations.
                                </div>
                            </div>
                            <div class="faq-item-answer">
                                <strong class="faq-item-label">Answer</strong>

                                <!-- 대답 -->
                                <div class="faq-item-content">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis debitis distinctio,
                                    explicabo, facere ad aperiam similique doloribus reiciendis nam, corporis veniam
                                    sequi dolorum. Eveniet accusantium officia earum tenetur? Harum, necessitatibus.
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="c-pagination-wrap" aria-label="pagination" role="navigation">
                        <ul class="c-pagination">
                            <li class="c-pagination-item">
                                <a class="c-pagination-link current" href="#">
                                    <span class="visuallyhidden">page </span>1
                                </a>
                            </li>
                            <li class="c-pagination-item">
                                <a class="c-pagination-link" href="#">
                                    <span class="visuallyhidden">page </span>2
                                </a>
                            </li>
                            <li class="c-pagination-item next-page">
                                <a class="c-pagination-link-arrow" href="#">
                                    <span class="visuallyhidden">Next page</span>
                                    <span class="sprite-direction arrow-to-right"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="faq-tab-panel tab-panel-trading" role="tabpanel">
                    <div class="faq-header">
                        <span class="faq-header-qna">Q/A</span>
                        <span class="faq-header-title">Title</span>
                    </div>
                    <ul class="faq-list js-faq-list">
                        <li class="faq-item">
                            <div class="faq-item-question">
                                <strong class="faq-item-label">Q<span class="faq-item-label-word">uestion</span></strong>

                                <!-- 질문 -->
                                <div class="faq-item-content">
                                    Wow awesome design. really like the illustrations.
                                </div>
                            </div>
                            <div class="faq-item-answer">
                                <strong class="faq-item-label">Answer</strong>

                                <!-- 대답 -->
                                <div class="faq-item-content">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis debitis distinctio,
                                    explicabo, facere ad aperiam similique doloribus reiciendis nam, corporis veniam
                                    sequi dolorum. Eveniet accusantium officia earum tenetur? Harum, necessitatibus.
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="c-pagination-wrap" aria-label="pagination" role="navigation">
                        <ul class="c-pagination">
                            <li class="c-pagination-item">
                                <a class="c-pagination-link current" href="#">
                                    <span class="visuallyhidden">page </span>1
                                </a>
                            </li>
                            <li class="c-pagination-item">
                                <a class="c-pagination-link" href="#">
                                    <span class="visuallyhidden">page </span>2
                                </a>
                            </li>
                            <li class="c-pagination-item next-page">
                                <a class="c-pagination-link-arrow" href="#">
                                    <span class="visuallyhidden">Next page</span>
                                    <span class="sprite-direction arrow-to-right"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="faq-tab-panel tab-panel-deposit" role="tabpanel">
                    <div class="faq-header">
                        <span class="faq-header-qna">Q/A</span>
                        <span class="faq-header-title">Title</span>
                    </div>
                    <ul class="faq-list js-faq-list">
                        <li class="faq-item">
                            <div class="faq-item-question">
                                <strong class="faq-item-label">Q<span class="faq-item-label-word">uestion</span></strong>

                                <!-- 질문 -->
                                <div class="faq-item-content">
                                    Wow awesome design. really like the illustrations.
                                </div>
                            </div>
                            <div class="faq-item-answer">
                                <strong class="faq-item-label">Answer</strong>

                                <!-- 대답 -->
                                <div class="faq-item-content">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis debitis distinctio,
                                    explicabo, facere ad aperiam similique doloribus reiciendis nam, corporis veniam
                                    sequi dolorum. Eveniet accusantium officia earum tenetur? Harum, necessitatibus.
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="c-pagination-wrap" aria-label="pagination" role="navigation">
                        <ul class="c-pagination">
                            <li class="c-pagination-item">
                                <a class="c-pagination-link current" href="#">
                                    <span class="visuallyhidden">page </span>1
                                </a>
                            </li>
                            <li class="c-pagination-item">
                                <a class="c-pagination-link" href="#">
                                    <span class="visuallyhidden">page </span>2
                                </a>
                            </li>
                            <li class="c-pagination-item next-page">
                                <a class="c-pagination-link-arrow" href="#">
                                    <span class="visuallyhidden">Next page</span>
                                    <span class="sprite-direction arrow-to-right"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="faq-tab-panel tab-panel-draw" role="tabpanel">
                    <div class="faq-header">
                        <span class="faq-header-qna">Q/A</span>
                        <span class="faq-header-title">Title</span>
                    </div>
                    <ul class="faq-list js-faq-list">
                        <li class="faq-item">
                            <div class="faq-item-question">
                                <strong class="faq-item-label">Q<span class="faq-item-label-word">uestion</span></strong>

                                <!-- 질문 -->
                                <div class="faq-item-content">
                                    Wow awesome design. really like the illustrations.
                                </div>
                            </div>
                            <div class="faq-item-answer">
                                <strong class="faq-item-label">Answer</strong>

                                <!-- 대답 -->
                                <div class="faq-item-content">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis debitis distinctio,
                                    explicabo, facere ad aperiam similique doloribus reiciendis nam, corporis veniam
                                    sequi dolorum. Eveniet accusantium officia earum tenetur? Harum, necessitatibus.
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="c-pagination-wrap" aria-label="pagination" role="navigation">
                        <ul class="c-pagination">
                            <li class="c-pagination-item">
                                <a class="c-pagination-link current" href="#">
                                    <span class="visuallyhidden">page </span>1
                                </a>
                            </li>
                            <li class="c-pagination-item">
                                <a class="c-pagination-link" href="#">
                                    <span class="visuallyhidden">page </span>2
                                </a>
                            </li>
                            <li class="c-pagination-item next-page">
                                <a class="c-pagination-link-arrow" href="#">
                                    <span class="visuallyhidden">Next page</span>
                                    <span class="sprite-direction arrow-to-right"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="faq-tab-panel tab-panel-etc" role="tabpanel">
                    <div class="faq-header">
                        <span class="faq-header-qna">Q/A</span>
                        <span class="faq-header-title">Title</span>
                    </div>
                    <ul class="faq-list js-faq-list">
                        <li class="faq-item">
                            <div class="faq-item-question">
                                <strong class="faq-item-label">Q<span class="faq-item-label-word">uestion</span></strong>

                                <!-- 질문 -->
                                <div class="faq-item-content">
                                    Wow awesome design. really like the illustrations.
                                </div>
                            </div>
                            <div class="faq-item-answer">
                                <strong class="faq-item-label">Answer</strong>

                                <!-- 대답 -->
                                <div class="faq-item-content">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis debitis distinctio,
                                    explicabo, facere ad aperiam similique doloribus reiciendis nam, corporis veniam
                                    sequi dolorum. Eveniet accusantium officia earum tenetur? Harum, necessitatibus.
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="c-pagination-wrap" aria-label="pagination" role="navigation">
                        <ul class="c-pagination">
                            <li class="c-pagination-item">
                                <a class="c-pagination-link current" href="#">
                                    <span class="visuallyhidden">page </span>1
                                </a>
                            </li>
                            <li class="c-pagination-item">
                                <a class="c-pagination-link" href="#">
                                    <span class="visuallyhidden">page </span>2
                                </a>
                            </li>
                            <li class="c-pagination-item next-page">
                                <a class="c-pagination-link-arrow" href="#">
                                    <span class="visuallyhidden">Next page</span>
                                    <span class="sprite-direction arrow-to-right"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <jsp:include page="/bitground/include/banner.jsp" flush="true"></jsp:include>

            </div>
            <!-- // .contents -->

        </div>
    </div>

    <jsp:include page="/bitground/include/footer.jsp" flush="true"></jsp:include>

    <script src="/bitground/js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/main.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>