<!doctype html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>bitground</title>
    <meta name="description" content="">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

    <link rel="manifest" href="/bitground/site.webmanifest">
    <link rel="apple-touch-icon" href="/bitground/icon.png">
    <link rel="shortcut icon" href="/bitground/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/bitground/css/base.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/common.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/main.css">
	<style type="text/css">
		.dim			{ position:absolute; top:0; left:0; width:100%; height:100%; background:rgba(0,0,0,0.8); font-size:40px; color:#fff; text-align:center; z-index:99999; line-height:113px; font-weight:bold; }
	</style>
</head>

<body class="page-main">
	
    <jsp:include page="/bitground/include/header.jsp" flush="true"></jsp:include>

    <div class="container">
        <div class="wrapper">
            <h2 class="visuallyhidden">Main Contents</h2>

            <div class="slider-wrap">
                <div class="slider-visual js-main-slider">
                    <div class="slider-visual-item app-open" data-title="Mobile App">
                        <img src="/bitground/img/main-visual-1.png" alt="BITGROUND Global Exchange Mobile App Open">
                        <div class="app-open-button-wrap">
                            <a href="#" class="app-open-button">
                                <img src="/bitground/img/google-play.png" alt="Get it on Google Play">
                            </a>
                            <a href="#" class="app-open-button">
                                <img src="/bitground/img/app-store.png" alt="Download on the App Store">
                            </a>
                        </div>
                    </div>
                    <div class="slider-visual-item" data-title="main Banner Generator">
                        <img src="/bitground/img/@temp-main-visual1.png" alt="">
                    </div>
                    <div class="slider-visual-item" data-title="main Banner">
                        <img src="/bitground/img/@temp-main-visual2.png" alt="">
                    </div>
                </div>
                <div class="slider-control">
                    <!-- <ul>
                        <li class="active"><button type="button" class="slider-control-button">Mobile App</button></li>
                        <li><button type="button" class="slider-control-button">Banner Generator</button></li>
                        <li><button type="button" class="slider-control-button">Banner</button></li>
                    </ul> -->
                </div>
            </div>
            <!-- // .slider-wrapper -->

            <div class="transaction-wrap">
                <div class="transaction-daily">
					<div class="dim">COMING SOON</div>
                    <h3 class="transaction-title">Transaction Amount</h3>
                    <div class="transaction-daily-line">
                        <h4 class="transaction-daily-time">Today</h4>
                        <ul class="transaction-daily-list">
                            <li>
                                <span class="t-primary">150,000,145,740.0000</span>
                                <em class="transaction-daily-label">PHP</em>
                            </li>
                            <li>
                                <span class="t-secondary">350,025,000,000.0000</span>
                                <em class="transaction-daily-label">BTC</em>
                            </li>
                            <li>
                                <span class="t-secondary">214,000,140,000.0000</span>
                                <em class="transaction-daily-label">ETH</em>
                            </li>
                        </ul>
                    </div>
                    <div class="transaction-daily-line">
                        <h4 class="transaction-daily-time">Yesterday</h4>
                        <ul class="transaction-daily-list">
                            <li>
                                <span>170,150,155,740.1586</span>
                                <em class="transaction-daily-label">PHP</em>
                            </li>
                            <li>
                                <span>240,245,000,218.0000</span>
                                <em class="transaction-daily-label">BTC</em>
                            </li>
                            <li>
                                <span>184,000,430,000.0000</span>
                                <em class="transaction-daily-label">ETH</em>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- // .transaction-daily -->

                <div class="transaction-kind">
                    <h3 class="visuallyhidden">All Coin Transaction</h3>
                    <ul class="transaction-kind-tablist js-transaction-tab" role="tablist">
                        <li class="transaction-kind-tab active" data-controls="tab-panel-php" role="tab">PHP</li>
                        <li class="transaction-kind-tab" data-controls="tab-panel-btc" role="tab">BTC</li>
                        <li class="transaction-kind-tab" data-controls="tab-panel-eth" role="tab">ETH</li>
                    </ul>
                    <div class="transaction-kind-tabpanel tab-panel-php" role="tabpanel" style="display: block;">
                        <p class="transaction-kind-total">
                            Total Volume
                            <span>000,000,000,000.0000 BTC</span>
                        </p>
                        <table class="transaction-kind-table">
                            <thead>
                                <tr>
                                    <th>Coin</th>
                                    <th>Buy</th>
                                    <th>Buy Amount</th>
                                    <th>Sell</th>
                                    <th>Sell Amount</th>
                                    <th>Volume</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><span class="coin-logo"><img src="/bitground/img/logo/BTC.png" alt="Bitcoin"></span>Bitcoin</td>
                                    <td>750,000,000.00</td>
                                    <td>750,000,000.00</td>
                                    <td>750,000,000.00</td>
                                    <td>750,000,000.00</td>
                                    <td>750,000,000.00</td>
                                </tr>
                                <tr>
                                    <td><span class="coin-logo"><img src="/bitground/img/logo/ETH.png" alt=""></span>Ethereum</td>
                                    <td>24,000,000.00</td>
                                    <td>24,000,000.00</td>
                                    <td>24,000,000.00</td>
                                    <td>24,000,000.00</td>
                                    <td>24,000,000.00</td>
                                </tr>
                                <tr>
                                    <td><span class="coin-logo"><img src="/bitground/img/logo/BCH.png" alt=""></span>Bitcoin
                                        Cash</td>
                                    <td>3,000.00</td>
                                    <td>3,000.00</td>
                                    <td>3,000.00</td>
                                    <td>3,000.00</td>
                                    <td>3,000.00</td>
                                </tr>
                                <tr>
                                    <td><span class="coin-logo"><img src="/bitground/img/logo/ETC.png" alt=""></span>Ethereum
                                        Classic</td>
                                    <td>24,000,000.00</td>
                                    <td>24,000,000.00</td>
                                    <td>24,000,000.00</td>
                                    <td>24,000,000.00</td>
                                    <td>24,000,000.00</td>
                                </tr>
                                <tr>
                                    <td><span class="coin-logo"><img src="/bitground/img/logo/XRP.png" alt=""></span>Ripple</td>
                                    <td>400,000.00</td>
                                    <td>400,000.00</td>
                                    <td>400,000.00</td>
                                    <td>400,000.00</td>
                                    <td>400,000.00</td>
                                </tr>
                                <tr>
                                    <td><span class="coin-logo"><img src="/bitground/img/logo/LTC.png" alt=""></span>Lite
                                        Coin</td>
                                    <td>157,000,000.00</td>
                                    <td>157,000,000.00</td>
                                    <td>157,000,000.00</td>
                                    <td>157,000,000.00</td>
                                    <td>157,000,000.00</td>
                                </tr>
                                <tr>
                                    <td><span class="coin-logo"><img src="/bitground/img/logo/ICX.png" alt=""></span>Icon</td>
                                    <td>14,000.00</td>
                                    <td>14,000.00</td>
                                    <td>14,000.00</td>
                                    <td>14,000.00</td>
                                    <td>14,000.00</td>
                                </tr>
                                <tr>
                                    <td><span class="coin-logo"><img src="/bitground/img/logo/EOS.png" alt=""></span>EOS</td>
                                    <td>200.00</td>
                                    <td>200.00</td>
                                    <td>200.00</td>
                                    <td>200.00</td>
                                    <td>200.00</td>
                                </tr>
                                <tr>
                                    <td><span class="coin-logo"><img src="/bitground/img/logo/ADA.png" alt=""></span>ADA</td>
                                    <td>4.00</td>
                                    <td>4.00</td>
                                    <td>4.00</td>
                                    <td>4.00</td>
                                    <td>4.00</td>
                                </tr>
                                <tr>
                                    <td><span class="coin-logo"><img src="/bitground/img/logo/QTUM.png" alt=""></span>Quantum</td>
                                    <td>453,000.00</td>
                                    <td>453,000.00</td>
                                    <td>453,000.00</td>
                                    <td>453,000.00</td>
                                    <td>453,000.00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="transaction-kind-tabpanel tab-panel-btc" role="tabpanel" style="display: none;">
                        <p class="transaction-kind-total">
                            Total Volume
                            <span>000,000,000,000.0000 BTC</span>
                        </p>
                        <table class="transaction-kind-table">
                            <thead>
                                <tr>
                                    <th>Coin</th>
                                    <th>Buy</th>
                                    <th>Buy Amount</th>
                                    <th>Sell</th>
                                    <th>Sell Amount</th>
                                    <th>Volume</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Bitcoin</td>
                                    <td>750,000,000.00</td>
                                    <td>750,000,000.00</td>
                                    <td>750,000,000.00</td>
                                    <td>750,000,000.00</td>
                                    <td>750,000,000.00</td>
                                </tr>
                                <tr>
                                    <td>Bitcoin Cash</td>
                                    <td>3,000.00</td>
                                    <td>3,000.00</td>
                                    <td>3,000.00</td>
                                    <td>3,000.00</td>
                                    <td>3,000.00</td>
                                </tr>
                                <tr>
                                    <td>Ethereum Classic</td>
                                    <td>24,000,000.00</td>
                                    <td>24,000,000.00</td>
                                    <td>24,000,000.00</td>
                                    <td>24,000,000.00</td>
                                    <td>24,000,000.00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="transaction-kind-tabpanel tab-panel-eth" role="tabpanel" style="display: none;">
                        <p class="transaction-kind-total">
                            Total Volume
                            <span>000,000,000,000.0000 BTC</span>
                        </p>
                        <table class="transaction-kind-table">
                            <thead>
                                <tr>
                                    <th>Coin</th>
                                    <th>Buy</th>
                                    <th>Buy Amount</th>
                                    <th>Sell</th>
                                    <th>Sell Amount</th>
                                    <th>Volume</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Bitcoin</td>
                                    <td>750,000,000.00</td>
                                    <td>750,000,000.00</td>
                                    <td>750,000,000.00</td>
                                    <td>750,000,000.00</td>
                                    <td>750,000,000.00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- // .transaction-kind -->
            </div>

            <div class="about-homepage">
                <div class="about-homepage-box about">
                    <h3 class="about-homepage-title">About BITGROUND</h3>
                    <p class="about-homepage-desc">
                        A team of engineers with years of<br />
                        experience has produced<br />
                        bitgounds.
                    </p>
                    <p class="about-homepage-desc">
                        Our technicians provide secure<br />
                        environments and ironclad security<br />
                        to our users.</p>
                </div>
                <div class="about-homepage-box exchange">
                    <h3 class="about-homepage-title">Exchange</h3>
                    <p class="about-homepage-desc">
                        You can see the change in coin<br />
                        prices through a chart operated by<br />
                        the exchange.
                    </p>
                    <p class="about-homepage-desc">
                        In addition to providing reliable<br />
                        chart data, BITGROUND have<br />
                        arranged to facilitate orders for<br />
                        purchase and sale.
                    </p>
                </div>
                <div class="about-homepage-box market">
                    <h3 class="about-homepage-title">Coin / PHP Market</h3>
                    <p class="about-homepage-desc">
                        You can find information about the<br />
                        coins that are deployed for each<br />
                        market.
                    </p>
                    <p class="about-homepage-desc">
                        You can also check the status of<br />
                        transactions by coin.
                    </p>
                </div>
                <div class="about-homepage-box service">
                    <h3 class="about-homepage-title">Our Service</h3>
                    <p class="about-homepage-desc">
                        We have built the most efficient, <br />
                        reliable and unique server security <br />
                        structure to deliver services.
                    </p>
                    <p class="about-homepage-desc">
                        And the exchange will make your <br />
                        coin deal enjoyable.
                    </p>
                </div>
            </div>

            <div class="notify-wrap">
                <div class="main-news">
                    <h3 class="main-news-title">News & Notice</h3>
                    <ul class="main-news-list">
                        <li class="notice">
                            <strong class="main-news-label">Notice</strong>
                            <a href="#" class="main-news-headline ellipsis">Binance Will Support CybCyberMiles
                                MainneerMiles Binance Will Support CybCyberMiles
                                MainneerMiles
                                (CMT) M</a>
                            <span class="main-news-time">2018-10-12</span>
                        </li>
                        <li class="notice">
                            <strong class="main-news-label">Notice</strong>
                            <a href="#" class="main-news-headline ellipsis">Will Support CyberMiles (CMCyberMiles T)
                                Mainnet
                                Token Swap</a>
                            <span class="main-news-time">2018-10-12</span>
                        </li>
                        <li>
                            <strong class="main-news-label">News</strong>
                            <a href="#" class="main-news-headline ellipsis">CyberMiles (CMT) Mainnet CyberMiles
                                MainneToken Swap</a>
                            <span class="main-news-time">2018-10-12</span>
                        </li>
                        <li>
                            <strong class="main-news-label">News</strong>
                            <a href="#" class="main-news-headline ellipsis">Support CyberMiles Mainnet TCyberMiles
                                Mainneoken
                                Swap</a>
                            <span class="main-news-time">2018-10-12</span>
                        </li>
                        <li>
                            <strong class="main-news-label">News</strong>
                            <a href="#" class="main-news-headline ellipsis">Will Support CyberMiles (CMT) Mai</a>
                            <span class="main-news-time">2018-10-12</span>
                        </li>
                        <li>
                            <strong class="main-news-label">News</strong>
                            <a href="#" class="main-news-headline ellipsis">Support CyberMiles Mainnet Token Swap</a>
                            <span class="main-news-time">2018-10-12</span>
                        </li>
                    </ul>
                    <a href="/bitground/html/notice.jsp" class="main-news-more">more</a>
                </div>

                <div class="contact-method">
                    <h3 class="contact-method-title">CS Center</h3>
                    <p class="contact-method-desc">
                        We currently operates only 1:1 Q&A boards.<br />
                        If you are uncomfortable, please touch the 1:1 inquiry button at<br />
                        the bottom to leave a message on the 1:1 Q&A board.
                    </p>
                    <p class="contact-method-desc">Weekday 10:00~19:00</p>
                    <p class="contact-method-inquiry">
                        <a href="#" class="c-button">1:1 inquiry</a>
                        <span>Send as an inquiry through the 1:1 inquiry,<br /> we will send you an answer</span>
                    </p>
                </div>
            </div>

            <div class="guide-homepage">
                <h3 class="guide-title">New to BITGROUND</h3>
                <ul class="guide-list">
                    <li><a href="#">How to sign up?</a></li>
                    <li><a href="#">How to Authenticate?</a></li>
                    <li><a href="#">Submit certificate</a></li>
                    <li><a href="#">Browse site</a></li>
                </ul>
            </div>

        </div>
    </div>

    <jsp:include page="/bitground/include/footer.jsp" flush="true"></jsp:include>

    <script src="/bitground/js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/main.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>