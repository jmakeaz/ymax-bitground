<!doctype html>
<%@ page contentType="text/html; charset=utf-8" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>bitground</title>
    <meta name="description" content="">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->

    <link rel="manifest" href="/bitground/site.webmanifest">
    <link rel="apple-touch-icon" href="/bitground/icon.png">
    <link rel="shortcut icon" href="/bitground/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="/bitground/css/base.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/common.css">
    <link rel="stylesheet" type="text/css" href="/bitground/css/board.css">
</head>

<body>
    <jsp:include page="/bitground/include/header.jsp" flush="true"></jsp:include>

    <div class="container">
        <div class="wrapper">
            <h2 class="visuallyhidden">Main Contents</h2>

            <div class="contents">
                <div class="c-page-header">
                    <h3 class="c-page-title">Terms of Service</h3>
                </div>
                <!-- // .c-page-header -->

                <div class="terms-wrap js-terms">
                    <ol class="terms-list js-terms-navi">
                        <li class="active">
                            <a href="#">1. Who May Use the Services</a>
                        </li>
                        <li>
                            <a href="#">2. Privacy</a>
                        </li>
                        <li>
                            <a href="#">3. Content on the Services</a>
                        </li>
                        <li>
                            <a href="#">4. Using the Services</a>
                        </li>
                        <li>
                            <a href="#">5. Disclaimers and Limitations of Liability</a>
                        </li>
                        <li>
                            <a href="#">6. General</a>
                        </li>
                    </ol>
                    <div class="terms-content">
                        <h4 class="terms-title">1. Who May Use the Services</h4>
                        <p class="terms-desc">You may use the Services only if you agree to form a binding contract
                            with Twitter and
                            are not a person barred from receiving services under the laws of the applicable
                            jurisdiction. In any case, you must be at least 13 years old, or in the case of
                            Periscope 16 years old, to use the Services. If you are accepting these Terms and using
                            the Services on behalf of a company, organization, government, or other legal entity,
                            you represent and warrant that you are authorized to do so and have the authority to
                            bind such entity to these Terms, in which case the words “you” and “your” as used in
                            these Terms shall refer to such entity.</p>
                        <h4 class="terms-title">2. Privacy</h4>
                        <p class="terms-desc">Our Privacy Policy (<a href="#">https://www.twitter.com/privacy</a>)
                            describes how we
                            handle the
                            information you provide to us when you use our Services. You understand that through
                            your use of the Services you consent to the collection and use (as set forth in the
                            Privacy Policy) of this information, including the transfer of this information to the
                            United States, Ireland, and/or other countries for storage, processing and use by
                            Twitter and its affiliates.</p>
                        <h4 class="terms-title">3. Content on the Services</h4>
                        <p class="terms-desc">You are responsible for your use of the Services and for any Content
                            you provide,
                            including
                            compliance with applicable laws, rules, and regulations. You should only provide
                            Content
                            that you are comfortable sharing with others.</p>
                        <p class="terms-desc">Any use or reliance on any Content or materials posted via the
                            Services or obtained by
                            you
                            through the Services is at your own risk. We do not endorse, support, represent or
                            guarantee the completeness, truthfulness, accuracy, or reliability of any Content or
                            communications posted via the Services or endorse any opinions expressed via the
                            Services.
                            You understand that by using the Services, you may be exposed to Content that might be
                            offensive, harmful, inaccurate or otherwise inappropriate, or in some cases, postings
                            that
                            have been mislabeled or are otherwise deceptive. All Content is the sole responsibility
                            of
                            the person who originated such Content. We may not monitor or control the Content
                            posted
                            via the Services and, we cannot take responsibility for such Content.</p>
                        <p class="terms-desc">We reserve the right to remove Content that violates the User
                            Agreement, including for
                            example, copyright or trademark violations, impersonation, unlawful conduct, or
                            harassment.
                            Information regarding specific policies and the process for reporting or appealing
                            violations can be found in our Help Center
                            (<a href="#">https://support.twitter.com/articles/15789#specific-violations</a> and
                            <a href="#">https://support.twitter.com/articles/15790</a>).</p>
                        <h4 class="terms-title">4. Privacy</h4>
                        <p class="terms-desc">Our Privacy Policy (<a href="#">https://www.twitter.com/privacy</a>)
                            describes how we
                            handle the
                            information you provide to us when you use our Services. You understand that through
                            your use of the Services you consent to the collection and use (as set forth in the
                            Privacy Policy) of this information, including the transfer of this information to the
                            United States, Ireland, and/or other countries for storage, processing and use by
                            Twitter and its affiliates.</p>
                        <h4 class="terms-title">5. Privacy</h4>
                        <p class="terms-desc">Our Privacy Policy (<a href="#">https://www.twitter.com/privacy</a>)
                            describes how we
                            handle the
                            information you provide to us when you use our Services. You understand that through
                            your use of the Services you consent to the collection and use (as set forth in the
                            Privacy Policy) of this information, including the transfer of this information to the
                            United States, Ireland, and/or other countries for storage, processing and use by
                            Twitter and its affiliates.</p>
                        <h4 class="terms-title">6. Privacy</h4>
                        <p class="terms-desc">Our Privacy Policy (<a href="#">https://www.twitter.com/privacy</a>)
                            describes how we
                            handle the
                            information you provide to us when you use our Services. You understand that through
                            your use of the Services you consent to the collection and use (as set forth in the
                            Privacy Policy) of this information, including the transfer of this information to the
                            United States, Ireland, and/or other countries for storage, processing and use by
                            Twitter and its affiliates.</p>
                    </div>
                </div>

                <jsp:include page="/bitground/include/banner.jsp" flush="true"></jsp:include>

            </div>
            <!-- // .contents -->

        </div>
    </div>

    <jsp:include page="/bitground/include/footer.jsp" flush="true"></jsp:include>

    <script src="/bitground/js/jquery-1.12.4.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="/bitground/js/main.js" type="text/javascript" charset="utf-8"></script>

</body>

</html>