<%@ page contentType="text/html; charset=utf-8" %>

<div class="c-search">
    <form action="#">
        <fieldset>
            <div class="c-combobox c-search-select">
                <span class="c-combobox-selection jsSearchCombo" role="combobox" aria-haspopup="true" aria-expanded="false"
                      tabindex="0" data-value="title">Title</span>
                <div class="c-combobox-results jsSearchOptions">
                    <ul class="c-combobox-list" role="listbox">
                        <li class="c-combobox-option" data-value="title" role="option" aria-selected="true">Title</li>
                        <li class="c-combobox-option" data-value="contents" role="option">Contents</li>
                        <li class="c-combobox-option" data-value="all" role="option">Title+Contents</li>
                    </ul>
                </div>
            </div>
            <span class="c-search-input">
                <input type="search" placeholder="Search">
            </span>
            <button type="button" class="c-search-button jsSearchButton">
                <span class="sprite-icon search"></span>
                <span class="visuallyhidden">Search</span>
            </button>
            <!-- // .c-search -->
        </fieldset>
    </form>
</div>
<!-- // .c-search -->