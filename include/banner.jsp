<%@ page contentType="text/html; charset=utf-8" %>

<div class="banner-wrap">
    <div class="banner-visual js-banner-slider">
        <div class="banner-visual-item app-open" data-title="Mobile App Open">
            <div><img src="/bitground/img/banner-app-store.png" alt="Bitground Global Exchange Mobile App Open"></div>
            <div class="banner-app-open-button-wrap">
                <a href="#" class="app-open-button">
                    <img src="/bitground/img/google-play.png" alt="Get it on Google Play">
                </a>
                <a href="#" class="app-open-button">
                    <img src="/bitground/img/app-store.png" alt="Download on the App Store">
                </a>
            </div>
        </div>
        <div class="banner-visual-item" data-title="Banner2" style="background-color: #97c6e3;">
            <img src="/bitground/img/@temp-banner1.png" alt="">
        </div>
        <div class="banner-visual-item" data-title="Banner3" style="background-color: #6b9dbc;">
            <img src="/bitground/img/@temp-banner2.png" alt="">
        </div>
    </div>
    <div class="banner-control">
        <!-- <ul>
            <li class="active"><button type="button" class="banner-control-button">Mobile App Open</button></li>
            <li><button type="button" class="banner-control-button">Banner Generator</button></li>
            <li><button type="button" class="banner-control-button">Banner</button></li>
        </ul> -->
    </div>
</div>
<!-- // .banner-wrapper -->