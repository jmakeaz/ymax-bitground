<%@ page contentType="text/html; charset=utf-8" %>

<div class="header">
    <div class="wrapper">
        <h1 class="logo">
            <a href="/bitground/html/">
                <img src="/bitground/img/bitground.png" alt="bitground" />
            </a>
        </h1>

        <div class="gnb-wrap">
            <h2 class="visuallyhidden">Main Menu</h2>
            <div class="gnb jsGnb">
                <ul>
                    <li class="gnb-item"><a href="/bitground/html/market.jsp">Market</a></li>
                    <li class="gnb-item"><a href="/bitground/html/exchange.jsp">Exchange</a></li>
                    <li class="gnb-item"><a href="#">CS Center</a>
                        <div class="snb">
                            <ul>
                                <li class="snb-item"><a href="/bitground/html/notice.jsp">Notice</a></li>
                                <li class="snb-item"><a href="/bitground/html/news.jsp">News</a></li>
                                <li class="snb-item"><a href="/bitground/html/information.jsp">Information</a></li>
                                <li class="snb-item"><a href="/bitground/html/faq.jsp">FAQ</a></li>
                                <li class="snb-item"><a href="/bitground/html/qna.jsp">Q&amp;A</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="user-menu-wrap">
            <h2 class="visuallyhidden">User Menu</h2>
            <div class="user-menu">
                <ul>
                    <!-- menu 활성화는 "active" 클래스명 추가 -->
                    <li class="active"><a href="/bitground/html/signin.jsp">Sign in</a></li>
                    <li><a href="/bitground/html/signup.jsp">Sign up</a></li>
                    <!-- <li><a href="/bitground/html/mypage.jsp">My Page</a></li>
                    <li><a href="#">Sign out</a></li> -->
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- // .header -->